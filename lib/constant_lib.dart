import 'package:flutter/material.dart';

const defaultColor = Color(0xFF1D1E33);
const inactiveColor = Color(0xFF111328);
const activeColor = Color(0xFF1D1E33);

const labelTextStyle = TextStyle(fontSize: 18.0, color: Color(0xFF8D8E98));
const numberTextStyle =
    TextStyle(fontSize: 50.0, fontWeight: FontWeight.w900, color: Colors.white);

const bottomContainerHeight = 80.0;

const kTitleTextStyle = TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold);
const kResultTextStyle = TextStyle(
  color: Colors.green,
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);
const kBMITextStyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
const kBodyTextStyle = TextStyle(
  fontSize: 22.0,
  color: Colors.white,
);

const kBottomTextStyle = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
);
