import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bmi_calculator/reusablecard.dart';
import 'package:bmi_calculator/icon_widget.dart';
import 'package:bmi_calculator/constant_lib.dart';
import 'package:bmi_calculator/roundiconbutton.dart';
import 'package:bmi_calculator/result_page.dart';
import 'package:bmi_calculator/bottom_container.dart';
import 'package:bmi_calculator/calculator.dart';

enum Gender { male, female }

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Color maleCardColor = inactiveColor;
  Color femaleCardColor = inactiveColor;
  int height = 180;
  int weight = 60;
  int age = 20;

  void updateCardColor(Gender selectedGender) {
    selectedGender == Gender.male
        ? maleCardColor = activeColor
        : maleCardColor = inactiveColor;
    selectedGender == Gender.female
        ? femaleCardColor = activeColor
        : femaleCardColor = inactiveColor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        updateCardColor(Gender.male);
                      });
                    },
                    child: IconWidget(
                      displayIcon: Icon(
                        FontAwesomeIcons.mars,
                        size: 80.0,
                      ),
                      displayText: "Male",
                      displayColor: maleCardColor,
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        updateCardColor(Gender.female);
                      });
                    },
                    child: IconWidget(
                      displayIcon: Icon(
                        FontAwesomeIcons.venus,
                        size: 80.0,
                      ),
                      displayText: "Female",
                      displayColor: femaleCardColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
          ReusableCard(
              cardColor: defaultColor,
              cardChild: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Height',
                    style: labelTextStyle,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                    textBaseline: TextBaseline.alphabetic,
                    children: <Widget>[
                      Text(
                        height.toString(),
                        style: numberTextStyle,
                      ),
                      Text(
                        'cm',
                        style: labelTextStyle,
                      ),
                    ],
                  ),
                  SliderTheme(
                    data: SliderTheme.of(context).copyWith(
                      activeTrackColor: Colors.white,
                      inactiveTrackColor: Color(0xFF8D8E98),
                      overlayColor: Color(0x29EB1555),
                      thumbColor: Color(0xFFEB1555),
                      thumbShape:
                          RoundSliderThumbShape(enabledThumbRadius: 15.0),
                      overlayShape:
                          RoundSliderOverlayShape(overlayRadius: 30.0),
                    ),
                    child: Slider(
                      value: height.toDouble(),
                      min: 100,
                      max: 220,
                      onChanged: (double newValue) {
                        setState(() {
                          height = newValue.round();
                        });
                      },
                    ),
                  ),
                ],
              )),
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ReusableCard(
                    cardColor: defaultColor,
                    cardChild: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Weight',
                          style: labelTextStyle,
                        ),
                        Text(
                          weight.toString(),
                          style: numberTextStyle,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: RoundIconButton(
                                icon: FontAwesomeIcons.minus,
                                onPressedFunction: () {
                                  setState(() {
                                    weight--;
                                  });
                                },
                              ),
                            ),
                            Expanded(
                              child: RoundIconButton(
                                icon: FontAwesomeIcons.plus,
                                onPressedFunction: () {
                                  setState(() {
                                    weight++;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ReusableCard(
                    cardColor: defaultColor,
                    cardChild: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Age',
                            style: labelTextStyle,
                          ),
                          Text(
                            age.toString(),
                            style: numberTextStyle,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RoundIconButton(
                                  icon: FontAwesomeIcons.minus,
                                  onPressedFunction: () {
                                    setState(() {
                                      age--;
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                  child: RoundIconButton(
                                icon: FontAwesomeIcons.plus,
                                onPressedFunction: () {
                                  setState(() {
                                    age++;
                                  });
                                },
                              )),
                            ],
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Calculator calc = Calculator(height: height, weight: weight);

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ResultPage(
                          bmiResult: calc.calculateBMI(),
                          resultText: calc.getResult(),
                          interpretation: calc.getInterpretation())));
            },
            child: BottomContainer(buttonTitle: 'Calculate'),
          )
        ],
      ),
    );
  }
}
