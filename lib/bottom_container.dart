import 'package:flutter/material.dart';
import 'package:bmi_calculator/constant_lib.dart';

class BottomContainer extends StatelessWidget {
  BottomContainer({required this.buttonTitle});

  final String buttonTitle;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFEB1555),
      margin: EdgeInsets.only(top: 10.0),
      width: double.infinity,
      height: 80.0,
      child: Center(
        child: Text(
          buttonTitle,
          style: kBottomTextStyle,
        ),
      ),
    );
  }
}
