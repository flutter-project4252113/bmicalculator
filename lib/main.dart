import 'package:flutter/material.dart';
import 'package:bmi_calculator/input_page.dart';

void main() => runApp(BMICalculator());

class BMICalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light().copyWith(
        primaryColor: Color(0xFFFFE082),
        scaffoldBackgroundColor: Color(0xFFB2EBF2),
        appBarTheme: AppBarTheme(
          color: Color(0xFFFFE082),
        ),
        buttonBarTheme: ButtonBarThemeData(
          buttonTextTheme: ButtonTextTheme.accent,
        ),
        textTheme: TextTheme(
          bodyText1: TextStyle(
            color: Color(0xFF212121),
          ),
          bodyText2: TextStyle(
            color: Color(0xFF212121),
          ),
        ),
      ),
      home: InputPage(),
    );
  }
}
