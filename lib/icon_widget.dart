import 'package:flutter/material.dart';
import 'reusablecard.dart';
import 'package:bmi_calculator/constant_lib.dart';

class IconWidget extends StatelessWidget {
  Icon displayIcon;
  String displayText;
  Color displayColor;

  IconWidget(
      {required this.displayIcon,
      required this.displayText,
      required this.displayColor});

  @override
  Widget build(BuildContext context) {
    return ReusableCard(
      cardColor: displayColor,
      cardChild: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          displayIcon,
          SizedBox(height: 15.0),
          Text(
            displayText,
            style: labelTextStyle,
          )
        ],
      ),
    );
  }
}
